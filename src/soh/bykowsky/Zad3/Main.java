package soh.bykowsky.Zad3;

public class Main {
    public static void main(String[] args) {
        Position p1 = new Position("Chleb", 1, 3.5);
        System.out.println(p1);
        Position p2 = new Position("Cukier", 3, 4);
        System.out.println(p2);
//        Order z = new Order(20);      // can implement this in constructor, but why? Currently number of positions is automaticaly updated based on every added position, which makes more sense
        Order z = new Order();
        z.addPosition(p1);
        z.addPosition(p2);
        System.out.println(z);
    }
}
