package soh.bykowsky.Zad3;


import java.util.ArrayList;
import java.util.List;

// klasa Zamowienie     (.pl)
public class Order {
    private List<Position> orderPositions;  // pozycje  (.pl)
    private int numberOfAddedPositions;     // ileDodanych  (.pl)

    public Order() {
        this.orderPositions = new ArrayList<>();
    }

    // void dodajPozycje(Pozycja p)     (.pl)
    public void addPosition(Position p){
        this.orderPositions.add(p);
        this.numberOfAddedPositions++;
    }

    // double obliczWartosc()   (.pl)
    public double calculateOrderValue(){

        double totalOrderValue = 0;

        for (Position p : orderPositions){
            totalOrderValue+= p.calculateTotalPositionPrice();
        }

        return totalOrderValue;
    }

    @Override
    public String toString() {
        String outputString = "\n";
        outputString += "Zamówienie:\n \n";

        for (Position p : orderPositions) {
            outputString += p + "\n";
        }

        outputString += "Razem: " + calculateOrderValue() + " zł";

        return outputString;
    }
}
