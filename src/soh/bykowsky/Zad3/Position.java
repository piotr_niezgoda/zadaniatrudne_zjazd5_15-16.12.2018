package soh.bykowsky.Zad3;

//klasa Pozycja     (.pl)
public class Position {
    private String name;            // nazwaTowaru  (.pl)
    private int numberOfPieces;     // ileSztuk     (.pl)
    private double pricePerPiece;   // cena         (.pl)

    public Position(String name, int numberOfPieces, double pricePerPiece) {
        this.name = name;
        this.numberOfPieces = numberOfPieces;
        this.pricePerPiece = pricePerPiece;
    }

    public double calculateTotalPositionPrice(){
        return pricePerPiece*numberOfPieces;
    }

    @Override
    public String toString() {
        return name +" " + pricePerPiece +
                " zł " + numberOfPieces + " szt. " + calculateTotalPositionPrice() + " zł";
    }
}
