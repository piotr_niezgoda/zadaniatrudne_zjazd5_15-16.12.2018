package soh.bykowsky.zad1;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        DatabaseDAO companyDatabase = new DatabaseDAOimplement();

        EmployeeManagmentSystem.printAllEmployees(companyDatabase);
        System.out.println("");

        EmployeeManagmentSystem.printManagerTeamMembers(3,companyDatabase);

        EmployeeManagmentSystem.addTaskForEmployee(new Task(8,"Housing MRB","Processing of the MRB",8, LocalDate.now()),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(8,"Seal MRB","Processing of the MRB",4, LocalDate.of(2019,1,7)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(8,"Seal Runner MRB","Processing of the MRB",4, LocalDate.of(2019,1,7)),companyDatabase);

        EmployeeManagmentSystem.addTaskForEmployee(new Task(9,"Housing LCF analysis","LCF Life analysis for No4 Bearing Housing - mesh preparation",8, LocalDate.of(2019,1,7)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(9,"Housing LCF analysis","LCF Life analysis for No4 Bearing Housing - mesh preparation",8, LocalDate.of(2019,1,8)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(9,"Housing LCF analysis","LCF Life analysis for No4 Bearing Housing- BC's preparation",8, LocalDate.of(2019,1,9)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(9,"Housing LCF analysis","LCF Life analysis for No4 Bearing Housing - model run",8, LocalDate.of(2019,1,10)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(9,"Housing LCF analysis","LCF Life analysis for No4 Bearing Housing - results postprocessing",8, LocalDate.of(2019,1,11)),companyDatabase);

        EmployeeManagmentSystem.addTaskForEmployee(new Task(10,"Clamped stack analysis","4B Spanner Nut Clamp stack analysis - model preparation",8, LocalDate.of(2019,1,7)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(10,"Clamped stack analysis","4B Spanner Nut Clamp stack analysis - model run",4, LocalDate.of(2019,1,8)),companyDatabase);
        EmployeeManagmentSystem.addTaskForEmployee(new Task(10,"Clamped stack analysis","4B Spanner Nut Clamp stack analysis - results postprocessing",4, LocalDate.of(2019,1,8)),companyDatabase);

        EmployeeManagmentSystem.addTaskForEmployee(11,"DDR pitch template","Preparing DDR pitch template", 8,LocalDate.of(2019,1,7),companyDatabase);

        System.out.println("");
        EmployeeManagmentSystem.addTaskForEmployee(7,"DDR pitch template","Preparing DDR pitch template", 8,LocalDate.of(2019,1,7),companyDatabase);

        System.out.println("***");
        EmployeeManagmentSystem.printManagerTeamMembersTasks(3,companyDatabase);

        System.out.println("*");
        EmployeeManagmentSystem.printTaskRegisterForAllLineWorkers(companyDatabase);

        EmployeeManagmentSystem.addTeamMember(3,6,companyDatabase);


    }
}
