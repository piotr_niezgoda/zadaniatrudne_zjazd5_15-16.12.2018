package soh.bykowsky.zad1;

import java.time.LocalDate;
import java.util.List;

public class EmployeeManagmentSystem {

    public static void printAllEmployees(DatabaseDAO database) {
        for (Employee employee : database.getAllEmployees()) {
            System.out.println(employee.toString());
        }
    }

    public static void printTaskRegisterForEmployee(int lineWorkerId, DatabaseDAO database) {

        if (database.getEmployee(lineWorkerId).getEmployeeType() == EmployeeType.LINE_WORKER) {
            for (Task task : database.getAllTasksPerEmployee(lineWorkerId)) {
                System.out.println(task.toString());
            }
        }
    }

    //TODO
    // Still need to work on this duplicated code. Any thoughts?
    public static void printTaskRegisterForAllLineWorkers(DatabaseDAO database) {
        for (Employee lineWorker : database.getAllLineWorkers()) {
            System.out.println("Id: " + lineWorker.getEmployeeId() + " name: " + lineWorker.getName() + " " + lineWorker.getSurename());

            List<Task> lineWorkerTasks = database.getAllTasksPerEmployee(lineWorker.getEmployeeId());

            if (lineWorkerTasks.size() != 0) {
                for (Task task : lineWorkerTasks) {
                    System.out.println(task.toString());
                }
            } else
                System.out.println("Employee has no tasks registered");

            System.out.println(" ");
        }
    }

    public static void printManagerTeamMembersTasks(int managerId, DatabaseDAO database) {
        for (Employee teamMember : database.getAllTeamMembers(managerId)) {
            System.out.println("Id: " + teamMember.getEmployeeId() + " name: " + teamMember.getName() + " " + teamMember.getSurename());

            List<Task> teamMemberTasks = database.getAllTasksPerEmployee(teamMember.getEmployeeId());

            if (teamMemberTasks.size() != 0) {
                for (Task task : teamMemberTasks) {
                    System.out.println(task.toString());
                }
            } else
                System.out.println("Employee has no tasks registered");

            System.out.println(" ");
        }
    }



    public static void printManagerTeamMembers(int managerId, DatabaseDAO database) {
        for (Employee teamMember : database.getAllTeamMembers(managerId)) {
            System.out.println(teamMember.toString());
        }
    }


    public static void addTaskForEmployee(Task task, DatabaseDAO database) {
        if (!database.addTask(task))
            System.out.println("Error. Task not added");
    }

    public static void addTaskForEmployee(int employeeId, String taskName, String taskDescription, int workedHours, LocalDate dateWhenWorked, DatabaseDAO database) {

        if (!database.addTask(new Task(employeeId, taskName, taskDescription, workedHours, dateWhenWorked)))
            System.out.println("Error. Task not added");
    }

    public static void addTeamMember(int managerId, int lineWorkerId, DatabaseDAO database){
        if (!database.addTeamMember(managerId,lineWorkerId))
            System.out.println("Error. Team member not added");
    }

}
