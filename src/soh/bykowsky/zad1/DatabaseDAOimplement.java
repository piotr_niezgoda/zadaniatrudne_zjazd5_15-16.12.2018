package soh.bykowsky.zad1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseDAOimplement implements DatabaseDAO {
    private static List<Employee> employeeList = new ArrayList<>();
    private static List<Task> tasks = new ArrayList<>();
    private static Map<Integer,Integer> teamList = new HashMap<>();    //key - employeeID, value - managerId --> employee with the same managerId are within the same team

    //TODO
    // Need to implement some data verification.
    // 1: Requirement is that managers have only lineworkers as teammembers - DONE

    //TODO
    // I rather think that input verification should be implemented here.

    static {
        employeeList.add(new Manager("Krzysztof", "Polonimski", Sex.MALE, 51, LocalDate.of(2003, 5, 1)));
        employeeList.add(new Manager("Michal", "Buczckowski", Sex.MALE, 43, LocalDate.of(2005, 1, 1)));
        employeeList.add(new Manager("Aleksandra", "Walinska", Sex.FEMALE, 41, LocalDate.of(2005, 1, 31)));
        employeeList.add(new Manager("Joanna", "Slonimska", Sex.FEMALE, 35, LocalDate.of(2009, 6, 1)));

        employeeList.add(new Accountant("Sarah", "Martin", Sex.FEMALE, 28, LocalDate.of(2010, 7, 1)));
        employeeList.add(new Accountant("James", "Reese", Sex.MALE, 28, LocalDate.of(2010, 2, 1)));
        employeeList.add(new Accountant("Karen", "Rampling", Sex.FEMALE, 31, LocalDate.of(2011, 1, 15)));

        employeeList.add(new LineWorker("Piotr", "Nowak", Sex.MALE, 32, LocalDate.of(2010, 10, 1)));
        employeeList.add(new LineWorker("Krzysztof", "Wisniewski", Sex.MALE, 41, LocalDate.of(2006, 4, 1)));
        employeeList.add(new LineWorker("Katarzyna", "Rybakowska", Sex.FEMALE, 32, LocalDate.of(2011, 1, 1)));
        employeeList.add(new LineWorker("Karol", "Roczkowski", Sex.MALE, 31, LocalDate.of(2011, 3, 1)));
        employeeList.add(new LineWorker("Maksymilian", "Zachocimski", Sex.MALE, 30, LocalDate.of(2012, 8, 1)));

        // creating team. puting into Map employees with the same managerId as values
        teamList.put(8,3);
        teamList.put(9,3);
        teamList.put(10,3);
        teamList.put(11,3);
        teamList.put(12,3);

    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeList;
    }

    @Override
    public Employee getEmployee(int employeeId) {
        for (Employee employee : employeeList) {
            if (employee.getEmployeeId() == employeeId)
                return employee;
        }
        return null;
    }

    @Override
    public List<Employee> getAllLineWorkers() {

        List<Employee> lineWorkerList = new ArrayList<>();

        for (Employee employee : employeeList) {
            if (employee.getEmployeeType() == EmployeeType.LINE_WORKER){
                lineWorkerList.add(employee);
            }
        }
        return lineWorkerList;
    }

    @Override
    public boolean addEmployee(Employee employee) {
        employeeList.add(employee);
        return true;
    }

    @Override
    public boolean updateEmployee(int employeeId, Employee employee) {
        for (Employee emp : employeeList) {
            if (emp.getEmployeeId() == employeeId) {
                employeeList.add(employeeId, employee);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteEmployee(int employeeId) {
        for (Employee employee : employeeList) {
            if (employee.getEmployeeId() == employeeId) {
                employeeList.remove(employeeId);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addTask(Task task) {

        for (Employee lineWorker : getAllLineWorkers()) {               // calling getAllLineWorkers() verifies that only line worker can add task.
            if (lineWorker.getEmployeeId() == task.getEmployeeId()){
                tasks.add(task);
                return true;
            }
        }
        return false;
    }

    @Override
    public Task getTask(int taskId) {
        for (Task task : tasks) {
            if (task.getTaskId() == taskId){
                return task;
            }
        }
        return null;
    }

    @Override
    public List<Task> getAllTasksPerEmployee(int employeeId) {

        List<Task> employeeTasks = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getEmployeeId() == employeeId)
                employeeTasks.add(task);
        }

        return employeeTasks;
    }

    @Override
    public List<Employee> getAllTeamMembers(int managerId) {
        List<Employee> teamMembers = new ArrayList<>();

        for (Map.Entry<Integer,Integer> entry: teamList.entrySet()){
            if (entry.getValue() == managerId){
                teamMembers.add(getEmployee(entry.getKey()));
            }
        }

        return teamMembers;
    }

    @Override
    public boolean addTeamMember(int managerId,int lineWorkerId) {

        // verification that provided id actually belongs to Manager
        boolean isManager = false;
        for (Employee employee : employeeList) {
            if (employee.getEmployeeId() == managerId){
                if (employee.getEmployeeType() == EmployeeType.MANAGER){
                    isManager = true;
                }
            }
        }

        // verification that provided id actually belongs to line worker
        boolean isLineWorker = false;
        for (Employee employee : employeeList) {
            if (employee.getEmployeeId() == lineWorkerId){
                if (employee.getEmployeeType() == EmployeeType.LINE_WORKER){
                    isLineWorker = true;
                }
            }
        }

        if (isLineWorker && isManager){
            teamList.put(lineWorkerId,managerId);
            return true;
        } else return false;
    }
}
