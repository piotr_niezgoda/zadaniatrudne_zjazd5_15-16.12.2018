package soh.bykowsky.zad1;

import java.util.List;

public interface DatabaseDAO {
    List<Employee> getAllEmployees();
    List<Employee> getAllLineWorkers();
    Employee getEmployee(int employeeId);
    boolean addEmployee(Employee employee);
    boolean updateEmployee(int employeeId, Employee employee);
    boolean deleteEmployee(int employeeId);

    boolean addTask(Task task);
    Task getTask(int taskId);
    List<Task> getAllTasksPerEmployee(int employeeId);

    List<Employee> getAllTeamMembers(int managerId);
    boolean addTeamMember(int managerId,int employeeId);
}
