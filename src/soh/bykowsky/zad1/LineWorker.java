package soh.bykowsky.zad1;

import java.time.LocalDate;

public class LineWorker extends Employee {
    public LineWorker(String name, String surename, Sex sex, int age, LocalDate hireDate) {
        super(name, surename, sex, EmployeeType.LINE_WORKER, age, hireDate);
    }
}
