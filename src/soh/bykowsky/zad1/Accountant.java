package soh.bykowsky.zad1;

import java.time.LocalDate;

public class Accountant extends Employee{

    public Accountant(String name, String surename, Sex sex, int age, LocalDate hireDate) {
        super(name, surename, sex, EmployeeType.ACCOUNTANT, age, hireDate);
    }
}
