package soh.bykowsky.zad1;

import java.time.LocalDate;

public class Task {
    private static int taskIdCounter;

    private int taskId;
    private int employeeId;
    private String taskName;
    private String taskDescription;
    private int workedHours;
    private LocalDate dateWhenWorked;



    public Task(int employeeId, String taskName, String taskDescription, int workedHours, LocalDate dateWhenWorked) {
        taskIdCounter++;
        this.taskId +=taskIdCounter;

        this.employeeId = employeeId;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.workedHours = workedHours;
        this.dateWhenWorked = dateWhenWorked;
    }


    @Override
    public String toString() {
        return "taskName='" + taskName + '\'' +
                ", taskDescription='" + taskDescription + '\'' +
                ", workedHours=" + workedHours +
                ", dateWhenWorked=" + dateWhenWorked +
                '}';
    }


    /**
     * GETTERS AND SETTERS
     */
    public int getTaskId() {
        return taskId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public int getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(int workedHours) {
        this.workedHours = workedHours;
    }

    public LocalDate getDateWhenWorked() {
        return dateWhenWorked;
    }

    public void setDateWhenWorked(LocalDate dateWhenWorked) {
        this.dateWhenWorked = dateWhenWorked;
    }
}
