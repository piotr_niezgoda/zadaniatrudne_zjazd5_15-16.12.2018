package soh.bykowsky.zad1;

public enum EmployeeType {
    MANAGER, ACCOUNTANT, LINE_WORKER;
}
