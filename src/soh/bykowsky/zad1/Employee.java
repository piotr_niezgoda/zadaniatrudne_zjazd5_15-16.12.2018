package soh.bykowsky.zad1;

import java.time.LocalDate;

public class Employee {
    private static int employeeIdCounter;

    private int employeeId;
    private String name;
    private String surename;
    private Sex sex;
    private EmployeeType employeeType;
    private int age;
    private LocalDate hireDate;


    public Employee(String name, String surename, Sex sex, EmployeeType employeeType, int age, LocalDate hireDate) {
        employeeIdCounter++;
        this.employeeId += employeeIdCounter;
        this.name = name;
        this.surename = surename;
        this.sex = sex;
        this.employeeType = employeeType;
        this.age = age;
        this.hireDate = hireDate;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", name='" + name + '\'' +
                ", surename='" + surename + '\'' +
                ", sex=" + sex +
                ", employeeType=" + employeeType +
                ", age=" + age +
                ", hireDate=" + hireDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (getEmployeeId() != employee.getEmployeeId()) return false;
        if (getAge() != employee.getAge()) return false;
        if (!getName().equals(employee.getName())) return false;
        if (!getSurename().equals(employee.getSurename())) return false;
        if (getSex() != employee.getSex()) return false;
        if (employeeType != employee.employeeType) return false;
        return getHireDate().equals(employee.getHireDate());
    }

    @Override
    public int hashCode() {
        int result = getEmployeeId();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getSurename().hashCode();
        result = 31 * result + getSex().hashCode();
        result = 31 * result + employeeType.hashCode();
        result = 31 * result + getAge();
        result = 31 * result + getHireDate().hashCode();
        return result;
    }

    /**
     * GETTERS AND SETTERS
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurename() {
        return surename;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    public int getEmployeeId() {
        return employeeId;
    }


}


