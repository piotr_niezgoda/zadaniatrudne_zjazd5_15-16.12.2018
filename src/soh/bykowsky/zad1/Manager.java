package soh.bykowsky.zad1;

import java.time.LocalDate;

public class Manager extends Employee {
    public Manager(String name, String surename, Sex sex, int age, LocalDate hireDate) {
        super(name, surename, sex, EmployeeType.MANAGER, age, hireDate);
    }
}


