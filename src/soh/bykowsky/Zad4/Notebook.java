package soh.bykowsky.Zad4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Notebook {
    private List<Note> notes;

    public Notebook() {
        this.notes = new ArrayList<>();
    }

    public void createNote(String noteBody){
        notes.add(new Note(noteBody));
    }

    public void createNote(String noteBody, String tag1){
        Set<String> tags = new HashSet<>();
        tags.add(tag1.toLowerCase());
        notes.add(new Note(tags,noteBody));
    }

    public void createNote(String noteBody, String tag1, String tag2){
        Set<String> tags = new HashSet<>();
        tags.add(tag1.toLowerCase());
        tags.add(tag2.toLowerCase());
        notes.add(new Note(tags,noteBody));
    }

    public void createNote(String noteBody, String tag1, String tag2, String tag3){
        Set<String> tags = new HashSet<>();
        tags.add(tag1.toLowerCase());
        tags.add(tag2.toLowerCase());
        tags.add(tag3.toLowerCase());
        notes.add(new Note(tags,noteBody));
    }

    public void createNote(String noteBody, Set<String> tags){
        notes.add(new Note(tags,noteBody));
    }
    //TODO Any more elegant way to deal with this multiple tags option???

    public List<Note> getAllNotes(){
        return notes;
    }

    public Note getNote(int noteId){
        for (Note note : notes) {
            if (note.getNoteId() == noteId)
                return note;
        }
        return null;
    }

    public List<Note> getNote(String word){
        List<Note> results = new ArrayList<>();

        for (Note note : notes) {
            if (note.isWordOrTagExist(word))
                results.add(note);

        }
        return results;
    }
}
