package soh.bykowsky.Zad4;

public class Main {
    public static void main(String[] args) {
        Notebook notebook = new Notebook();

        notebook.createNote("Need to implement some extra things in home exercise 4");
        notebook.createNote("Shopping after work");
        notebook.createNote("mySQL practice is still waiting");

        notebook.createNote("Need posting about selling by bike", "Motorcycle");
        notebook.createNote("Need to prepare my bike for the season. Need basics service. By oil and oil filter", "Motorcycle");
        notebook.createNote("Need to fix wife's bike","Motorcycle");

        notebook.createNote("Some books about SPRING in mind","SPRING", "JAVA");


        System.out.println(notebook.getNote(1));
        System.out.println(notebook.getNote(2));
        System.out.println(notebook.getNote(4));

        System.out.println("********************");

        for (Note note : notebook.getAllNotes()) {
            System.out.println(note);
        }

        System.out.println("********************");

        System.out.println(notebook.getNote("Shopping"));
        System.out.println(notebook.getNote("Motorcycle"));

    }
}
