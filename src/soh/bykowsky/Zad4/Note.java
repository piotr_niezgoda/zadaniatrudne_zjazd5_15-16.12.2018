package soh.bykowsky.Zad4;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Note {
    private static int noteIdCounter;

    private int noteId;
    private Set<String> tags;
    private String noteBody;
    private LocalDate date;

    public Note(String noteBody) {
        noteIdCounter++;
        this.noteId += noteIdCounter;

        this.noteBody = noteBody;
        this.date = LocalDate.now();
        Set<String> tags = new HashSet<>();
    }

    public Note(Set<String> tags, String noteBody) {
        noteIdCounter++;
        this.noteId += noteIdCounter;

        this.tags = tags;
        this.noteBody = noteBody;
        this.date = LocalDate.now();
    }


    public boolean isWordOrTagExist(String word) {

        word = word.toLowerCase();

        if (tags == null) {
            if (noteBody.toLowerCase().contains(word))
                return true;
            return false;
        } else {

            if (noteBody.toLowerCase().contains(word) || tags.contains(word))
                return true;
            return false;
        }
    }

    @Override
    public String toString() {
        if (isTagsNull()) {
            return "Note{" +
                    "noteId=" + noteId +
                    ", noteBody='" + noteBody + '\'' +
                    ", date=" + date +
                    '}';
        } else {
            return "Note{" +
                    "noteId=" + noteId +
                    ", tags=" + tags +
                    ", noteBody='" + noteBody + '\'' +
                    ", date=" + date +
                    '}';
        }
    }

    private boolean isTagsNull() {
        if (tags == null)
            return true;
        return false;
    }

    public int getNoteId() {
        return noteId;
    }

    public Set<String> getTags() {
        return tags;
    }

    public String getNoteBody() {
        return noteBody;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public void setNoteBody(String noteBody) {
        this.noteBody = noteBody;
    }
}
