package soh.bykowsky.Zad5;

public class Rectangle implements Figure{
    private String figureType;
    private double rectangleSideA;
    private double rectangleSideB;

    public Rectangle(double rectangleSideA, double rectangleSideB) {
        this.figureType = "Rectangle";
        this.rectangleSideA = rectangleSideA;
        this.rectangleSideB = rectangleSideB;
    }

    @Override
    public double getPerimeter() {
        return (2* rectangleSideA + 2* rectangleSideB);
    }

    @Override
    public double getArea() {
        return (rectangleSideA * rectangleSideB);
    }

    // GETTERS AND SETTERS
    public double getRectangleSideA() {
        return rectangleSideA;
    }

    public double getRectangleSideB() {
        return rectangleSideB;
    }

    @Override
    public String getFigureType() {
        return figureType;
    }
}
