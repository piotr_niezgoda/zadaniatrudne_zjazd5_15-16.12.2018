package soh.bykowsky.Zad5;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Provide circle's radius (double): ");
            Circle circle = new Circle(scanner.nextDouble());

            System.out.println("");
            System.out.println("Triangle next");
            System.out.print("Provide length of the first side adjacent to the right angle: ");
            double firstSideAdjacentToRightAngle = scanner.nextDouble();

            System.out.print("Provide length of the second side adjacent to the right angle: ");
            double SecondSideAdjacentToRightAngle = scanner.nextDouble();

            Triangle triangle = new Triangle(firstSideAdjacentToRightAngle, SecondSideAdjacentToRightAngle);

            System.out.println("");
            System.out.println("Finally rectangle");
            System.out.print("Provide side a: ");
            double rectangleSideA = scanner.nextDouble();

            System.out.print("Provide side b: ");
            double rectangleSideB = scanner.nextDouble();
            System.out.println("");

            Rectangle rectangle = new Rectangle(rectangleSideA, rectangleSideB);

            Figure[] figuresArray = {circle, triangle, rectangle};

            DecimalFormat outputFormatting = new DecimalFormat("#.##");
            String formattedOutput = new String();

            for (Figure figure : figuresArray) {
                formattedOutput = outputFormatting.format(figure.getArea());
                System.out.println(figure.getFigureType() + "'s area: " + formattedOutput);

                formattedOutput = outputFormatting.format(figure.getPerimeter());
                System.out.println(figure.getFigureType() + "'s perimeter: " + formattedOutput);

                System.out.println(" ");
            }
        } catch (InputMismatchException e) {
            System.out.println("Problem with inputs");
            e.printStackTrace();
        }
    }
}
