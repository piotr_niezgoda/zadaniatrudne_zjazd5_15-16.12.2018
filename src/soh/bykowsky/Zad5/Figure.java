package soh.bykowsky.Zad5;

public interface Figure {
     final double PI = 3.14;

    double getPerimeter();
    double getArea();

    String getFigureType();
}
