package soh.bykowsky.Zad5;

public class Circle implements Figure {
    private String figureType;
    double radius;

    public Circle(double radius) {
        this.radius = radius;
        this.figureType = "Circle";
    }

    @Override
    public double getPerimeter() {
        return (this.radius*2*PI);
    }

    @Override
    public double getArea() {
        return (PI*this.radius*this.radius);
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public String getFigureType() {
        return figureType;
    }
}
