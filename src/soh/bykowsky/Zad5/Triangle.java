package soh.bykowsky.Zad5;

public class Triangle implements Figure {   // right angle triangle
    private String figureType;
    double firstSideAdjacentToRightAngle;
    double secondSideAdjacentToRightAngle;

    public Triangle(double firstSideAdjacentToRightAngle, double secondSideAdjacentToRightAngle) {
        this.firstSideAdjacentToRightAngle = firstSideAdjacentToRightAngle;
        this.secondSideAdjacentToRightAngle = secondSideAdjacentToRightAngle;
        this.figureType = "Triangle";
    }

    @Override
    public double getPerimeter() {
        double sideOppositeToRightAngle = Math.sqrt((firstSideAdjacentToRightAngle*firstSideAdjacentToRightAngle)+(secondSideAdjacentToRightAngle*secondSideAdjacentToRightAngle));
        return sideOppositeToRightAngle+firstSideAdjacentToRightAngle+secondSideAdjacentToRightAngle;
    }

    @Override
    public double getArea() {
        return (firstSideAdjacentToRightAngle * secondSideAdjacentToRightAngle /2);
    }

    @Override
    public String getFigureType() {
        return figureType;
    }
}
