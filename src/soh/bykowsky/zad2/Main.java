package soh.bykowsky.zad2;

public class Main {
    public static void main(String[] args) {
        DatabaseDAO database = new DatabaseDAOimplement();

        database.listNewlyRegisteredPackages();

        database.assignPackageToCourier(1,1);
        database.assignPackageToCourier(3,1);
        database.assignPackageToCourier(2,2);
        database.assignPackageToCourier(4,2);
        database.assignPackageToCourier(7,3);
        database.assignPackageToCourier(8,3);

        database.listNewlyRegisteredPackages();

        database.markPackageAsDelivered(1);
        database.markPackageAsDelivered(2);
        database.markPackageAsDelivered(3);

        database.listNotDeliveredPackagesFromAll();

        database.listPackagesNotDeliveredByCourier(1);
        database.listPackagesNotDeliveredByCourier(2);
        database.listPackagesNotDeliveredByCourier(3);



    }
}
