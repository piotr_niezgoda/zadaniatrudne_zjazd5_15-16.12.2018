package soh.bykowsky.zad2;

public interface DatabaseDAO {
    void listNotDeliveredPackagesFromAll();

    void listPackagesNotDeliveredByCourier(int courierId);

    void listNewlyRegisteredPackages();

    void assignPackageToCourier(int packageId, int courierId);

    void markPackageAsDelivered(int packageId);
}
