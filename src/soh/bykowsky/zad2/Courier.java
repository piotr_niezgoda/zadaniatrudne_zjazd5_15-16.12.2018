package soh.bykowsky.zad2;

import java.util.Objects;

public class Courier {
    private static int courierIdCounter;

    private int courierId;
    private String name;
    private String surename;
    private int regionId;

    public Courier(String name, String surename, int regionId) {
        courierIdCounter++;

        this.courierId = courierIdCounter;
        this.name = name;
        this.surename = surename;
        this.regionId = regionId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courier courier = (Courier) o;
        return getRegionId() == courier.getRegionId() &&
                Objects.equals(getName(), courier.getName()) &&
                Objects.equals(getSurename(), courier.getSurename());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurename(), getRegionId());
    }

    @Override
    public String toString() {
        return "Courier{" +
                "name='" + name + '\'' +
                ", surename='" + surename + '\'' +
                ", regionId=" + regionId +
                '}';
    }

    /**
     * GETTERS AND SETTERS
     */
    public String getName() {
        return name;
    }

    public String getSurename() {
        return surename;
    }

    public int getRegionId() {
        return regionId;
    }

    public int getCourierId() {
        return courierId;
    }
}
