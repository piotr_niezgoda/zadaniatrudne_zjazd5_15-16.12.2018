package soh.bykowsky.zad2;

//TODO
// Don't we need to implement separate class for printig data ona screen and keep
// DAOimplement to only deal with database???

import java.util.*;

public class DatabaseDAOimplement implements DatabaseDAO {

    /************************************************
     * Database simulation
     */
    private static List<Package> newlyRegisteredPackages = new LinkedList<>();
    private static Map<Package, Courier> sortedPackageDatabase = new HashMap<>();
    private static List<Courier> courierList = new ArrayList<>();

    static {
        newlyRegisteredPackages.add(registerPackage(new Adress("Piaseczynska 27", "3", "03-375", "Poland", "048 591 701 116")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Hrubieszowska 4", "12-242", "Poland")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Witosa 18", "22-675", "Poland")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Wyndtree Avenue 412", "12", "222-223", "US")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Konwiktorska 25", "2", "11-235", "Poland", "048 234 777 666")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Konwiktorska 25", "3", "11-235", "Poland", "048 661 012 260")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Konwiktorska 27", "8", "11-235", "Poland", "048 812 170 200")));
        newlyRegisteredPackages.add(registerPackage(new Adress("Al. Niepodleglosci 25", "1", "27-071", "Poland", "048 648 860 106")));

        courierList.add(new Courier("Piotr", "Namyslowski", 3));
        courierList.add(new Courier("Marcin", "Walaszek", 3));
        courierList.add(new Courier("Michał", "Stoiński", 3));
    }

    private static Package registerPackage(Adress adress) {
        return new Package(adress);
    }

    /**
     *
     *************************************************/


    @Override
    public void listNotDeliveredPackagesFromAll() {
        System.out.println("All not delivered packages:");
        System.out.println("----------------------------");

        for (Package aPackage : newlyRegisteredPackages) {
            System.out.println(aPackage.toString());
        }

        System.out.println("");
        for (Map.Entry<Package, Courier> entry : sortedPackageDatabase.entrySet()) {
            if (entry.getKey().getPackageStatus() != PackageStatus.DELIVERED) {
                System.out.print(entry.getKey());
                System.out.print("  Assigned to: ");
                System.out.println(entry.getValue());
                System.out.println("");
            }
        }
    }

    @Override
    public void listPackagesNotDeliveredByCourier(int courierId) {

        boolean noCourierWithSuchIdExist = true;
        for (Courier courier : courierList) {
            if (courier.getCourierId() == courierId) {
                noCourierWithSuchIdExist = false;

                if (sortedPackageDatabase.containsValue(courier)) {
                    System.out.println("Courier " + courier.getName() + " " + courier.getSurename());

                    boolean allDelivered = true;

                    //todo Can we optimise this?
                    for (Map.Entry<Package, Courier> entry : sortedPackageDatabase.entrySet()) {
                        if (entry.getKey().getPackageStatus() != PackageStatus.DELIVERED) {
                            if (entry.getValue().equals(courier)) {
                                System.out.println(entry.getKey());
                                allDelivered = false;
                            }
                        }
                    }

                    if (allDelivered == true) {
                        System.out.println("All packages delivered");
                    }
                }

            }
        }

        if (noCourierWithSuchIdExist == true)
            System.out.println("No courier with such ID exist in database");

    }

    @Override
    public void listNewlyRegisteredPackages() {
        System.out.println("All newly registered and not sent yet packages:");
        System.out.println("----------------------------");

        for (Package newlyRegisteredPackage : this.newlyRegisteredPackages) {
            System.out.println(newlyRegisteredPackage.toString());
        }

        System.out.println("");
    }

    @Override
    public void assignPackageToCourier(int packageId, int courierId) {
        ListIterator<Package> listIterator = this.newlyRegisteredPackages.listIterator();

        boolean noPackageWithSuchIDexist = true;
        while (listIterator.hasNext()) {
            if (listIterator.next().getPackadeId() == packageId) {
                noPackageWithSuchIDexist = false;

                boolean noCourierWithSuchIDexist = true;
                for (Courier courier : courierList) {
                    if (courier.getCourierId() == courierId) {
                        sortedPackageDatabase.put(newlyRegisteredPackages.get(listIterator.previousIndex()), courier);
                        newlyRegisteredPackages.get(listIterator.previousIndex()).setPackageStatus(PackageStatus.SENT_FOR_DELIVERY);
                        listIterator.remove();
                        noCourierWithSuchIDexist = false;
                    }
                }

                if (noCourierWithSuchIDexist == true)
                    System.out.println("No courier with such ID exist in database");

            }
        }

        if (noPackageWithSuchIDexist == true)
            System.out.println("No package with such ID exist in database");
    }

    @Override
    public void markPackageAsDelivered(int packageId) {
        for (Map.Entry<Package, Courier> entry : sortedPackageDatabase.entrySet()) {
            if (entry.getKey().getPackadeId() == packageId) {
                entry.getKey().setPackageStatus(PackageStatus.DELIVERED);
            }
        }
    }
}
