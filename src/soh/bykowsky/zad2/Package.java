package soh.bykowsky.zad2;

import java.util.Objects;

public class Package {
    private static int packageIdCounter;

    private int packageId;
    private PackageStatus packageStatus;
    private Adress adress;

    public Package(Adress adress) {
        packageIdCounter++;
        this.packageId = packageIdCounter;
        this.packageStatus = PackageStatus.REGISTERED;
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Package{" +
                "packageId=" + packageId +
                ", packageStatus=" + packageStatus +
                ", "+ adress.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return packageId == aPackage.packageId &&
                Objects.equals(getAdress(), aPackage.getAdress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageId, getAdress());
    }

    /**
     * GETTERS AND SETTERS
     */
    public int getPackadeId() {
        return packageId;
    }

    public PackageStatus getPackageStatus() {
        return packageStatus;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setPackageStatus(PackageStatus packageStatus) {
        this.packageStatus = packageStatus;
    }
}
