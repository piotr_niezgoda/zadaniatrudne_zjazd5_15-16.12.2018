package soh.bykowsky.zad2;

public class Adress {
    private String street;
    private String apartment;
    private String zip;
    private String country;
    private String phoneNumber;

    public Adress(String street, String zip, String country) {
        this.street = street;
        this.apartment = "Not provided";
        this.zip = zip;
        this.country = country;
        this.phoneNumber = "Not provided";
    }

    public Adress(String street, String apartment, String zip, String country) {
        this.street = street;
        this.apartment = apartment;
        this.zip = zip;
        this.country = country;
        this.phoneNumber = "Not provided";
    }

    public Adress(String street, String apartment, String zip, String country, String phoneNumber) {
        this.street = street;
        this.apartment = apartment;
        this.zip = zip;
        this.country = country;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "street='" + street + '\'' +
                ", apartment='" + apartment + '\'' +
                ", zip='" + zip + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }


    /**
     * GETTERS AND SETTERS
     */
    public String getStreet() {
        return street;
    }

    public String getApartment() {
        if (this.apartment.equals(null))
            return "not provided";
        else
            return apartment;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public String getPhoneNumber() {
        if (this.phoneNumber.equals(null))
            return "not provided";
        else
            return phoneNumber;
    }

}
